
/*
Good work here, Lakshmi.   Your application works well
and you have nice regex validation on your email.  You also
have the comments I Needed to see.  I did miss a couple of things
though:
- the validate() function should have been named validateForm()
- when I don't enter an email, the focus isn't set back to the email txtbox
Nice work otherwise
18/20
*/

// array list of province

function loadProvinces()
    {
    var provArray=["Alberta", "British Columbia", "Manitoba", "New Brunswick", "Newfoundland and Labrador", "Nova Scotia", "Ontario" , "Prince Edward Island" , "Quebec","Saskatchewan"];
    var output="<option value=''>-Select-</option>";
    for(index=0;index<provArray.length;index++)
    {
        output+="<option value='"+provArray[index]+"'>"+provArray[index]+"</option>";
    }
    document.getElementById('cboProvince').innerHTML=output;
    }
// form Validation

function validate()
    {
    var name = document.getElementById('txtName').value;
    var email = document.getElementById('txtEmail').value;
    var province = document.getElementById('cboProvince').value;
    // formatting variables
    var emailFormat = /(\w+\.)*\w+@([a-zA-Z]+\.)+[a-zA-Z]{2,6}/;
    if(name==""){
        alert('Please enter a Name');
        document.getElementById('txtName').focus();
        return false;
    }

    if(email==""){
        alert("please enter an Email.");
        return;
    }else{
       if(!emailFormat.test(email)){
       alert("please enter a valid email format");
       return;
    }
    }

    if(province==""){
        alert('Please select a Province');
        document.getElementById('cboProvince').focus();
        return false;
    }


    alert('Congrats! Form entered successfully');
}
